package tasks

import "fmt"

func RemoveDuplicates(input []string) []string {
	/*
		output := make([]string, len(input))
		    copy(output, input)

		    inputSet := make(map[string]struct{}, len(input))
		    outputIdx := 0
		    for _, v := range input {
		        if _, ok := inputSet[v]; !ok {
		            output[outputIdx] = v
		            outputIdx++
		        }
		        inputSet[v] = struct{}{}
		    }

		    return output[:outputIdx]
	*/
	set := map[string]struct{}{}

	for k := range input {
		set[input[k]] = struct{}{}
	}

	input = input[:0]

	for k := range set {
		input = append(input, k)
	}

	return input
}

func PrintSlice() {
	input := []string{
		"cat",
		"dog",
		"bird",
		"dog",
		"parrot",
		"cat",
	}

	fmt.Println(RemoveDuplicates(input))
}
