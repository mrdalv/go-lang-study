package tasks

import (
	"log"
	"os"
)

type LogLevel int

const (
	LogLevelError LogLevel = iota
	LogLevelWarning
	LogLevelInfo
)

func (l LogLevel) IsValid() bool {
	switch l {
	case LogLevelInfo, LogLevelError, LogLevelWarning:
		return true
	default:
		return false
	}
}

type LogExtended struct {
	*log.Logger
	logLevel LogLevel // LogLevel это enum
}

func (le *LogExtended) SetLogLevel(logLvl LogLevel) {
	if !logLvl.IsValid() {
		return
	}
	le.logLevel = logLvl
}

func (le *LogExtended) Infoln(msg string) {
	le.println(LogLevelInfo, "Info: ", msg)
}

func (le *LogExtended) Warnln(msg string) {
	le.println(LogLevelWarning, "WARN: ", msg)
}

func (le *LogExtended) Errorln(msg string) {
	le.println(LogLevelError, "ERR: ", msg)
}

func (le *LogExtended) println(srcLogLvl LogLevel, prefix, msg string) {
	if le.logLevel < srcLogLvl {
		return
	}

	le.Logger.Println(prefix + msg)
}

func NewLogExtended() *LogExtended {
	return &LogExtended{
		Logger:   log.New(os.Stderr, "", log.LstdFlags),
		logLevel: LogLevelError,
	}
}

func Exec() {
	logger := NewLogExtended()
	logger.SetLogLevel(LogLevelWarning)
	logger.Infoln("Не должно напечататься")
	logger.Warnln("Hello")
	logger.Errorln("World")
	logger.Println("Debug")
}
