package tasks

import (
	"bytes"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

func WhitePixels() {
	if len(os.Args) < 2 {
		fmt.Println(`Укажите имя .png или .jpg файла`)
		return
	}
	imagefile, err := os.ReadFile(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}
	img, _, err := image.Decode(bytes.NewReader(imagefile))
	if err != nil {
		fmt.Println(err)
		return
	}
	bounds := img.Bounds()
	var whiteCount int
	for x := bounds.Min.X; x < bounds.Max.X; x++ {
		for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
			r, g, b, _ := img.At(x, y).RGBA()
			if r>>8 == 0xff && g>>8 == 0xff && b>>8 == 0xff {
				whiteCount++
			}
		}
	}
	fmt.Println(whiteCount)
}
