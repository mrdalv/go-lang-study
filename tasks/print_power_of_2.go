package tasks

import (
	"fmt"
	"math/bits"
)

func IsPowerOf2(x uint) (power int, ok bool) {
	ok = (x & (x - 1)) == 0
	if ok {
		power = bits.TrailingZeros(x)
	}
	return
}

func PrintPowerOf2(x uint) {
	power, ok := IsPowerOf2(5)
	if ok {
		fmt.Println(power)
	} else {
		fmt.Println("No")
	}
}
