package tasks

import "fmt"

func MapTesting() {
	frequency := map[string]int{
		"хлеб":     50,
		"молоко":   100,
		"масло":    200,
		"колбаса":  500,
		"соль":     20,
		"огурцы":   200,
		"сыр":      600,
		"ветчина":  700,
		"буженина": 900,
		"помидоры": 250,
		"рыба":     300,
		"хамон":    1500,
	}

	for k, v := range frequency {
		if v > 500 {
			fmt.Printf("Дороже 500р %s \n", k)
		}
	}

	s := []string{"хлеб", "буженина", "сыр", "огурцы"}

	var sum int

	for _, v := range s {
		sum += frequency[v]
	}

	fmt.Printf("Стоимость заказа - %d", sum)
	// for _, v := range sentence {
	// 	frequency[v]++ // встреченному знаку увеличиваем его частоту
	// }
	// печатаем
	// for k, v := range frequency {
	// 	fmt.Printf("Знак %c встречается %d раз \n", k, v)
	// }
}
