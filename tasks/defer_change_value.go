package tasks

import "fmt"

var Global = 5

func DeferChangeValue() {
	// defer func(checkout int) {
	//     Global = checkout
	// }(Global)
	// Global = 22
	defer func() { Global = 5 }()
	Global = 1000
	fmt.Println(Global)
}

func PrintGlobalValue() {
	fmt.Println(Global)
	DeferChangeValue()
	fmt.Println(Global)
}
