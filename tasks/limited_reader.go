package tasks

import (
	"bytes"
	"io"
	"log"
	"os"
	"strings"
)

func limitReader(r io.Reader, n int) io.Reader {
	byteStorage := make([]byte, n)

	if _, err := io.ReadFull(r, byteStorage); err != nil {
		log.Fatal(err)
	}

	return bytes.NewReader(byteStorage)
}

/*
type LimitedReader struct {
    reader io.Reader
    left   int
}

func LimitReader(r io.Reader, n int) io.Reader {
    return &LimitedReader{reader: r, left: n}
}

func (r *LimitedReader) Read(p []byte) (int, error) {
    if r.left == 0 {
        return 0, io.EOF
    }
    if r.left < len(p) {
        p = p[0:r.left]
    }
    n, err := r.reader.Read(p)
    r.left -= n
    return n, err
}
*/

func MyLimetedRead() {
	r := strings.NewReader("some io.Reader stream to be read\n")
	lr := limitReader(r, 7)

	_, err := io.Copy(os.Stdout, lr)
	if err != nil {
		log.Fatal(err)
	}
}
