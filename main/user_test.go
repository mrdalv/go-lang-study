package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

type DBStorage interface {
	UserExists(email string) bool
}

func NewUser(db DBStorage, email string) error {
	if db.UserExists(email) {
		return fmt.Errorf(`user with '%s' email already exists`, email)
	}
	// добавляем запись
	return nil
}

type DBMock struct {
	emails map[string]bool
}

func (db *DBMock) UserExists(email string) bool {
	return db.emails[email]
}

func (db *DBMock) addUser(email string) {
	db.emails[email] = true
}

func TestNewUser(t *testing.T) {
	errPattern := `user with '%s' email already exists`
	tbl := []struct {
		name    string
		email   string
		preset  bool
		wanterr bool
	}{
		{`want success`, `gregorysmith@myexampledomain.com`, false, false},
		{`want error`, `johndoe@myexampledomain.com`, true, true},
	}
	for _, item := range tbl {
		t.Run(item.name, func(t *testing.T) {
			dbMock := &DBMock{emails: make(map[string]bool)}
			if item.preset {
				dbMock.addUser(item.email)
			}
			err := NewUser(dbMock, item.email)
			if !item.wanterr {
				require.NoError(t, err)
			} else {
				require.EqualError(t, err, fmt.Sprintf(errPattern, item.email))
			}
		})
	}
}