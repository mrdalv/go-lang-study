package main

import (
	"testing"
)

func TestFooFunc(t *testing.T) {
	expectedFooResult := "MrDalv"
	if actualFooResult := Foo(); actualFooResult != expectedFooResult {
		t.Errorf("expected %s; got: %s", expectedFooResult, actualFooResult)
	}
}

func TestAdd(t *testing.T) {
	if sum := Add(10, 3); sum != 13 {
		t.Errorf("sum expected to be 13; got %d", sum)
	}
}
