package main

import (
	k "gitlab.com/mrdalv/go-lang-study/tasks"
)

func Foo() string {
	return "MrDalv"
}

func Add(a, b int) int {
	return a + b
}

func main() {
	// ShowHelloWorld()
	// BranchingOperators()
	// FizzBuzz()
	// CountLines()
	// SliceFill()
	// MapTesting()
	// PrintSlice()
	// Serialize()
	// PrintPowerOf2(2)
	// ReadArea(square, 5)
	// k.PrintGlobalValue()
	// k.WhitePixels()
	// k.Recursive()
	// k.RealFunc()
	// k.CircularBufferMain()
	// k.Exec()
	// k.PrintUserEmail(k.User{Email: "mrdalv@ya.ru"})
	// k.ReaderExample()
	// k.WriterExample()
	// k.CheckSum()
	// k.MyLimetedRead()
	// k.RealizeCheckToday()
	// k.ParseTags()
	k.ValidateType()
}
