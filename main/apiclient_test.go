package main

import (
	"errors"
	"testing"
)

type APIClient interface {
	GetData(query string) (Response, error)
}

type Response struct {
	Text       string
	StatusCode int
}

type MockAPIClient interface {
	APIClient
	SetResponse(resp Response, err error)
}

type Mock struct {
	Resp Response
	Err  error
}

func (m *Mock) GetData(query string) (Response, error) {
	return m.Resp, m.Err
}

func (m *Mock) SetResponse(resp Response, err error) {
	m.Resp = resp
	m.Err = err
}

func TestAPIClient(t *testing.T) {
	tbl := []struct {
		query    string
		response Response
		err      error
	}{
		{
			"success", Response{"OK", 200}, nil,
		},
		{
			"error", Response{"", 500}, errors.New("Something is wrong"),
		},
		{
			"empty", Response{"", 404}, nil,
		},
	}

	for _, item := range tbl {
		m := &Mock{}
		m.SetResponse(item.response, item.err)
		resp, err := m.GetData(item.query)

		if resp.Text != item.response.Text {
			t.Errorf(`%s: want %v got %v`, item.query, item.response.Text, resp.Text)
		}
		if !errors.Is(err, item.err) {
			t.Errorf(`%s: want %v got %v`, item.query, item.err, err)
		}
	}
}
